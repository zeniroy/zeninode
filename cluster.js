
var numCPUs = require('os').cpus().length;

var redis = require("redis");
var client = redis.createClient();
client.on("error", function (err) {
	console.log("Error " + err);
});

var z = require("zeninode")();
var _ = require("lodash");
var async = require("async");

var c = {};

c.initCluster = function() {
	if(!c.id) {
		c.id = makeId(10);
		c.maxId = -1;
	}
};

c.resetClusterData = function() {
	c.id = makeId(10);
	c.maxId = -1;
};

c.setClusterData = function(dataId, data, cb) {
	c.initCluster();
	z.p("set data {0}_data{1}".format(c.id, dataId));
	client.set('{0}_data{1}'.format(c.id, dataId), JSON.stringify(data), cb);
	if(c.maxId < dataId) {
		c.maxId = dataId;
	}
};

c.startClustering = function(cluster, func_name, exchange, cb) {
	c.initCluster();
	var workers = [];
	var currId = 0;
	function allocProc() {
		if(currId <= c.maxId) {
			var worker = cluster.fork({id:c.id, currId:currId++, maxId:c.maxId, func_name:func_name, exchange:exchange});
			workers.push(worker);
		}
	}

	for (var i = 0; i < numCPUs; i++) {
		allocProc();
	}

	if(exchange) {
		cluster.on('exit', function(worker, code, signal) {
			z.p('worker ' + worker.process.pid + ' died');
			_.remove(workers, function(currWorker) {
				return currWorker.process.pid === worker.process.pid;
			});

			if(signal === -1) {
				cb({errCode:'Child died -1'});
			} else {
				if(currId > c.maxId) {
					if(workers.length <= 0) {
						cb();
					}
				} else {
					allocProc();
				}
			}
		});
	} else {
		client.set('{0}_currId'.format(c.id), -1, function(){
			cluster.on('exit', function(worker, code, signal) {
				if(signal === -1) {
					cb({errCode:'Child died -1'});
				} else {
					z.p('worker ' + worker.process.pid + ' died');
					_.remove(workers, function(currWorker) {
						return currWorker.process.pid === worker.process.pid;
					});
					if(workers.length <= 0) {
							cb();
					}
				}
			});
		});
	}

	if(workers.length === 0) {
		cb();
	}
};

c.clusterWorker = function(jobs) {
	if(process.env.exchange === "true") {
		client.get('{0}_data{1}'.format(process.env.id, process.env.currId), function(err, result){
			if(err) {
				z.e(err);
				process.exit(-1);
			} else {
				jobs[process.env.func_name](JSON.parse(result), function(err){
					if(err) {
						z.e(err);
						process.exit(-1);
					} else {
						process.exit();
					}
				});
			}
		});
	} else {
		function clientProc() {
			async.waterfall(z.async.task([
			function(cb) {
				client.incr('{0}_currId'.format(process.env.id), cb);
			}, function(cb, currId) {
				if(parseInt(process.env.maxId) <= parseInt(currId)) { // exit
					z.p("complte process");
					process.exit();
				} else {
					z.p("{0}/{1} processing..".format(currId, process.env.maxId));
					client.get('{0}_data{1}'.format(process.env.id, currId), cb);
				}
			}, function(cb, results) {
				jobs[process.env.func_name](JSON.parse(results), cb);
			}
			]), function(err) {
				if(err) {
					z.e(err);
					process.exit(-1);
				} else {
					clientProc();
				}
			});
		}
		clientProc();
	}
};

function makeId(maxLen)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < maxLen; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

module.exports = c;
