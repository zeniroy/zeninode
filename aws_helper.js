
var z = require("zeninode")();
var _ = require("lodash");

var aws = {};

aws.ec2 = {};
aws.ec2.run_instances = function(ec2, ssh, param, cb) {
	if(!param.Monitoring || typeof param.Monitoring.Enalbed === "undefined") {
		param.Monitoring = {Enabled:false};
	}
	if(!param.MinCount) param.MinCount = param.MaxCount;

    z.aw([
    function(cb) { // 서버 n개를 띄운다.
		ec2.runInstances(param, cb);
    }, function(cb, data) { // running이 될때까지 기다린다.
		setTimeout(function(){
			aws.ec2.wait_for_running(ec2, ssh, data.Instances, cb);
		}, 3000);	
    }], cb);
};

aws.ec2.request_spot = function(ec2, ssh, param, initFunc, cb) {
	this.requestIds = null;
	this.initiatedInstances = [];
	this.param = param;
	this.initFunc = initFunc;
	this.ec2 = ec2;
	this.ssh = ssh;
	this.init(cb);
};

aws.ec2.request_spot.prototype.getMinPrice = function(cb) {
	var _this = this;
	_this.ec2.describeSpotPriceHistory({
		InstanceTypes: [_this.param.LaunchSpecification.InstanceType],
		ProductDescriptions: ['Linux/UNIX'],
		MaxResults: 100
	}, function(err, data){
		if(err) cb(err);
		else {
			if(data.SpotPriceHistory.length === 0) z.end({error:true, message:"no history"});

			var minPrice = _.min(_.map(_.pairs(_.groupBy(data.SpotPriceHistory, function(x) {
				return x.AvailabilityZone;
			})), function(x) {
				var totalValue = _.sum(x[1], function(x) {
					return parseFloat(x.SpotPrice);
				});
				return totalValue/x[1].length;
			}));

			cb(null, minPrice);
		}
	});
};

aws.ec2.request_spot.prototype.init = function(cb) {
	var _this = this;
	_this.start(cb);
	_this.update();
};

aws.ec2.request_spot.prototype.updateParam = function(cb) {
	var _this = this;
	if(!_this.param.LaunchSpecification.Monitoring) {
		_this.param.LaunchSpecification.Monitoring = {Enabled: false};
	}

	if(!_this.param.SpotPrice) {
		_this.getMinPrice(function(err, minPrice) {
			if(err) cb(err);
			else {
				_this.param.SpotPrice = (minPrice * 1.2).toString();
				cb();
			}
		});
	} else {
		cb();
	}
};

aws.ec2.request_spot.prototype.start = function(cb) {
	var _this = this;
	z.aw([
		function(cb) {
			if(_this.requestIds) _this.end(cb);
			else cb();
		}, function(cb) {
			_this.updateParam(cb);
		}, function(cb) {
			_this.ec2.requestSpotInstances(_this.param, cb);
		}, function(cb, data) {
			_this.requestIds = _.map(data.SpotInstanceRequests, function(x) { return x.SpotInstanceRequestId; });
			cb();
		}
	], cb);
};

aws.ec2.request_spot.prototype.update = function() {
	var _this = this;
	var checkTime = new Date();
	var zeroPosition = false;
	setInterval(function() {
		if(_this.requestIds) {
			z.aw([
				function(cb) {
					_this.ec2.describeSpotInstanceRequests({SpotInstanceRequestIds: _this.requestIds}, cb);
				}, function(cb, data) {
					var currActiveIntances = _.filter(_.map(data.SpotInstanceRequests, function(x) { return x.InstanceId; }), function(x) { return x; });
					var newInstanceIds = _.difference(currActiveIntances, _this.initiatedInstances);
					z.p("newInstanceIds");
					z.p(newInstanceIds);
					
					if(!zeroPosition) {
						if(currActiveIntances.length === 0) {
							zeroPosition = true;
							checkTime = new Date();
						}
					} else {
						if(currActiveIntances.length > 0) {
							zeroPosition = false;
						} else {
							if(new Date() - checkTime > 180000) {
								_this.param.SpotPrice = null;
								_this.start(function(err) {if(err) z.end(err);});
								checkTime = new Date();
							}
						}
					}

					_this.initiatedInstances = currActiveIntances;
					if(newInstanceIds.length > 0) {
						z.aw([
							function(cb) {
								_this.ec2.describeInstances({InstanceIds: currActiveIntances}, cb);
							}, function(cb, data) {
								var activeInstances = _.flatten(_.map(data.Reservations, function(reservation) {
									return reservation.Instances;	
								}));
								var newInstances = _.filter(activeInstances, function(instance) {
									return _.contains(newInstanceIds, instance.InstanceId);
								});
								aws.ec2.wait_for_running(_this.ec2, _this.ssh, newInstances, z.pass(cb, activeInstances));
							}, function(cb, activeInstances, newInstances) {
								_this.initFunc(newInstances, activeInstances, cb);
							}
						], cb);
					} else {
						cb();
					}
				}
			], function(err){
				if(err) z.e(err);
			});
		}
	}, 10000);
};

aws.ec2.request_spot.prototype.end = function(cb) {
	var _this = this;
	z.aw([
		function(cb) {
			_this.ec2.describeSpotInstanceRequests({SpotInstanceRequestIds: _this.requestIds}, cb);
		}, function(cb, data) {
			var currActiveIntances = _.filter(_.map(data.SpotInstanceRequests, function(x) { return x.InstanceId; }), function(x) { return x; });
			_this.ec2.cancelSpotInstanceRequests({SpotInstanceRequestIds: _this.requestIds}, z.pass(cb, currActiveIntances));
			_this.requestIds = null;
		}, function(cb, currActiveIntances) {
			if(currActiveIntances.length > 0) {
				_this.ec2.terminateInstances({InstanceIds:currActiveIntances}, cb);		
			} else cb();
		}
	], cb);
};

aws.ec2.wait_for_running = function(ec2, ssh, instances, cb) {
	ec2.describeInstances({
		InstanceIds:_.map(instances, function(instance){
			return instance.InstanceId;
		})
	}, function(err, data){
		if(err) {
			z.e(err);
			setTimeout(function(){
				aws.ec2.wait_for_running(ec2, ssh, instances, cb);
			}, 0);
			return;
		}

		var activeInstances = _.flatten(_.map(data.Reservations, function(reservation) {
			return reservation.Instances;	
		}));

		var addressed_instances = _.filter(activeInstances, function(instance) {
			return instance.PublicDnsName !== "";
		});

		if(addressed_instances.length < data.Reservations[0].Instances.length) {
			setTimeout(function(){
				aws.ec2.wait_for_running(ec2, ssh, instances, cb);
			}, 5000);
		} else {
			var tasks = [];
            _.forEach(addressed_instances, function(instance) {
				z.at(tasks, function(cb, addr) {
					ssh.cmd(addr, "ls -alt", cb);
				}, instance.PublicDnsName);
			});
			z.aw(tasks, function(err) {
				if(err) {
					setTimeout(function(){
						aws.ec2.wait_for_running(ec2, ssh, instances, cb);
					}, 5000);
				} else {
					cb(null, addressed_instances);
				}
			});
		}
	});	
};

aws.ec2.terminateInstances = function(ec2, instances, cb) {
    ec2.terminateInstances({InstanceIds:_.map(instances, function(x){return x.InstanceId;})}, cb);
};

aws.dyn = {};
aws.dyn.wait_for_active = function(dynamodb, tableName, cb) {
	dynamodb.describeTable({TableName:tableName}, function(err, data){
		if(err) cb(err);
		else {
			if(data.Table.TableStatus !== 'ACTIVE') {
				setTimeout(function(){
					aws.dyn.wait_for_active(dynamodb, tableName, cb);
				}, 5000);
			} else {
				cb(null, data);
			}
		}
	});
};

aws.dyn.wait_for_end_deleting = function(dynamodb, tableName, cb) {
	dynamodb.describeTable({TableName:tableName}, function(err, data){
		if(err) cb(null, err);
		else {
			if(data.Table.TableStatus === 'DELETING') {
				setTimeout(function(){
					aws.dyn.wait_for_end_deleting(dynamodb, tableName, cb);
				}, 5000);
			} else {
				cb({message:'delete failed', status:data.Table.TableStatus});
			}
		}
	});
};

aws.dyn.delete_table = function(dynamodb, tableName, cb) {
	z.aw([
	function(cb) {
		z.p('delete table');
		dynamodb.deleteTable({TableName:tableName}, function(err, data){
			cb(null);
		});
	}, function(cb) {
		z.p('delete checking...');
		aws.dyn.wait_for_end_deleting(dynamodb, tableName, cb);
	}], cb);
};

aws.dyn.create_table = function(dynamodb, tableName, keyName, read, write, cb) {
	z.aw([
	function(cb) {
		z.p('create table');
		var params = {
			AttributeDefinitions: [ // required
				{
					AttributeName: keyName, // required
					AttributeType: 'S'
				}
			],
			KeySchema: [ // required
				{
					AttributeName: keyName, // required
					KeyType: 'HASH' // required
				}
			],
			ProvisionedThroughput: { // required
				ReadCapacityUnits: read,// required
				WriteCapacityUnits: write// required
			},
			TableName: tableName // required
		};
		dynamodb.createTable(params, cb);
	}, function(cb) {
		z.p('create checking...');
		z.dyn.wait_for_active(tableName, cb);
	}], cb);
};

aws.dyn.updateThroughput = function(dynamodb, tableName, read, write, waitForActive, cb) {
	if(typeof waitForActive === 'function') {
		cb = waitForActive;
		waitForActive = true;
	}

	z.aw([
	function(cb) {
		aws.dyn.wait_for_active(dynamodb, tableName, cb);
	}, function(cb, data) {
		if(data.Table.ProvisionedThroughput.ReadCapacityUnits !== read || data.Table.ProvisionedThroughput.WriteCapacityUnits !== write) {
			dynamodb.updateTable({
				TableName:tableName, 
				ProvisionedThroughput:{
					ReadCapacityUnits:read,
					WriteCapacityUnits:write
				}
			}, cb);
		} else {
			cb();
		}
	}, function(cb, updateData) {
		if(waitForActive && updateData) {
			aws.dyn.wait_for_active(dynamodb, tableName, cb);
		} else {
			cb();
		}
	}], cb);
};

aws.spot = {};
aws.spot.getMinPrice = function(AWS, config, region, type, cb) {
	var ec2 = new AWS.EC2(_.assign(config, {apiVersion:"2014-10-01", region:region}));
	ec2.describeSpotPriceHistory({
		InstanceTypes: [type],
		ProductDescriptions: ['Linux/UNIX'],
		MaxResults: 100 
	}, function(err, data){
		if(err) z.end(err);
		else {
			if(data.SpotPriceHistory.length === 0) z.end({error:true, message:"no history"});

			var currDatas = _.map(_.pairs(_.groupBy(data.SpotPriceHistory, function(x) {
				return x.AvailabilityZone;
			})), function(x) {
				var totalValue = _.sum(x[1], function(x) {
					return parseFloat(x.SpotPrice);
				});
				return [x[0], totalValue/x[1].length];
			});
			var totalMean = _.sum(currDatas, function(x) { return x[1]; })/currDatas.length;
			var minPrice = _.min(currDatas, function(x) { return x[1]; });
			minPrice.push(totalMean);
			cb(null, minPrice);
		}
	});
};

aws.spot.getMinMaxPrice = function(AWS, config, region, type, cb) {
	var ec2 = new AWS.EC2(_.assign(config, {apiVersion:"2014-10-01", region:region}));
	ec2.describeSpotPriceHistory({
		InstanceTypes: [type],
		ProductDescriptions: ['Linux/UNIX'],
		MaxResults: 100
	}, function(err, data){
		if(err) z.end(err);
			else {
				if(data.SpotPriceHistory.length === 0) z.end({error:true, message:"no history"});

				var currDatas = _.map(_.pairs(_.groupBy(data.SpotPriceHistory, function(x) {
					return x.AvailabilityZone;
				})), function(x) {
					var maxValue = _.max(x[1], function(x) {
						return parseFloat(x.SpotPrice);
					});
					return [x[0], parseFloat(maxValue.SpotPrice)];
				});
				var minPrice = _.min(currDatas, function(x) { return x[1]; });
				cb(null, minPrice);
			}
	});
};

aws.spot.selectRegionAndPrice = function(AWS, config, instanceType, cb) {
	var regions = ["us-east-1", "us-west-1", "us-west-2", "ap-northeast-1", "ap-southeast-1", "ap-southeast-2", "sa-east-1", "eu-west-1", "eu-central-1"];
	var regionDatas = [];
	z.aw([
		function(cb) {
			z.aw_task(regions, function(region, cb) {
				aws.spot.getMinPrice(AWS, config, region, instanceType, function(err, minPrice) {
					if(err) cb(err);
					else {
						regionDatas.push(minPrice);
						cb();
					}
				});
			}, cb);
		}, function(cb) {
			cb(null, _.min(_.filter(regionDatas, function(x) { return x[2]/x[1] < 1.2; }), function(x) { return x[1]; }));
		}
	], cb);
};

aws.emr = {};
aws.emr.createCluster = function(emr, name, masterType, coreType, coreNum, bidPrice, keypair, subnetId, cb) {
	var params = {
		  Instances: {
			Ec2KeyName: keypair,
		  	Ec2SubnetId: subnetId,
			InstanceGroups: [
				{
					InstanceRole: "MASTER",
					InstanceType: masterType,
					InstanceCount: 1,
					Market: "SPOT",
					BidPrice: bidPrice.toFixed(2)+"",
					Name: "EMR MASTER"
				}, 
				{
					InstanceRole: "CORE",
					InstanceType: coreType,
					Market: "SPOT",
					BidPrice: bidPrice.toFixed(2)+"",
					InstanceCount: coreNum,
					Name: "EMR CORE"
				}
			],
			KeepJobFlowAliveWhenNoSteps: true
		  },
		  Name: name,
		  ReleaseLabel: 'emr-4.2.0',
		  Applications: [
			  {
				  Name: "Spark"
			  },
			  {
				  Name: "Hadoop"
			  }
		  ],
		  JobFlowRole: 'EMR_EC2_DefaultRole',
		  ServiceRole: 'EMR_DefaultRole'
	};
	emr.runJobFlow(params, function(err, data) {
		if(err) z.end(err);
		else cb(null, data);
	});
};

module.exports = aws;

