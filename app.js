
var _ = require('lodash');
var async = require('async');
var fs = require('fs');
var spawn = require('child_process').spawn;

var z = {};


/**
 * toStr - string이 아닌 object들을 JSON.stringify로 자동 변환해주는 함수
 *
 * @param  {object} data 변환할 데이터
 * @return {string}      string 으로 변환한 데이터
 */
function toStr(data) { return typeof data === 'string' ? data:JSON.stringify(data); }

/**
 * p - print를 편하게 하기 위한 wrapper
 *
 * @param  {object} obj print할 object
 * @return {object}     chaining을 위해 받은 인자를 그대로 return
 */
z.p = function(obj) {console.log(new Date()+" "+toStr(obj));return obj;}

/**
 * err - 에러 로깅을 편하게 하기 위한 wrapper
 *
 * @param  {object} obj print할 error
 * @return {object}     chaining을 위해 받은 인자를 그대로 return
 */
z.err = function(obj) {console.error(new Date()+" "+toStr(obj));return obj;}

// underscore를 워낙 많이 쓰니까 여기서 공유할 수 있도록
z._ = _;


/**
 * e - error의 stack을 print하기 위한 helper
 *
 * @param  {object} err print할 error
 * @return {object}     chaining을 위해 받은 인자를 그대로 return
 */
z.e = function(err) {
	if (err) {
		if(typeof err === 'object') {
			if (err.message) {
				z.err('\nMessage: ' + err.message);
			}
			if (err.stack) {
				z.err('\nStacktrace:');
				z.err('====================');
				z.err(err.stack);
			}
			if(!err.message && !err.stack) {
				z.err(err);
			}
			return err;
		} else {
			return z.err(err);
		}
	} else return err;
};

/**
 * fnlog - 현재 context를 호출한 함수를 추적할 때 사용하는 함수. 지금보니 잘못된 함수.
 *
 * @return {undefined}
 */
z.fnlog = function() {
	var caller = arguments.callee.caller.toString();
	caller = caller.substr('function '.length);
	caller = caller.substr(0, caller.indexOf('('));
	var args = Array.prototype.slice.call(arguments)[0];
	args = _.mapValues(args,function(val){
		if(typeof val !== "function") {
			return val;
		} else {
			return String(val).substr(0,15).replace(/\n/g,"")+"...";
		}
	});
	z.p("function "+caller+"("+_.values(args)+")");
};

z.async = {
	// tasks라는 task function들이 들어있는 array에 cb의 인자 순서를 맞춰서 taskFunc를 넣어준다.
	taskInjector : function (tasks, taskFunc) {
		var user_args = Array.prototype.slice.call(arguments).slice(2);
		tasks.push(function() {
			var args = Array.prototype.slice.call(arguments);
			taskFunc.apply(this, args.slice(args.length-1).concat(user_args).concat(args.slice(0, args.length-1)));
		});
	},
	// function을 array를 받아서 taskInjector를 한꺼번에 해주는 것과 같은 효과를 만드는 함수
	task : function(funcs) {
		return _.map(funcs, function(func){
			return function(){
				var args = Array.prototype.slice.call(arguments);
				func.apply(this, args.slice(args.length-1).concat(args.slice(0, args.length-1)));
			};
		});
	},
	// error가 없는 경우 null을 맨 처음 인자로 넘겨줘야하는데 그것을 명시적으로 보여주기 위해서 만든 함수인데, 잘 쓰고 있지는 않음.
	noerr: function(cb) {
		return function() {
			var args = [null].concat(Array.prototype.slice.call(arguments));
			cb.apply(this, args);
		};
	}
};

z.mysql = {
	/**
	 * connect - mysql의 connection을 만드는 함수
	 *
	 * @param  {object} db_config db 연결 정보
	 * @param  {function} cb        callback
	 * @return {undefined}
	 */
	connect : function(db_config, cb) {
		var mysql = require('mysql');
		var	connection = mysql.createConnection(db_config); // Recreate the connection, since

		// log query
		connection.config.queryFormat = function(query, value) {
			z.p(query);
			return query;
		};

		// the old one cannot be reused.
		connection.connect(function(err){
			if(err) cb(err);
			else cb(null, connection);
		});                                     // process asynchronous requests in the meantime.
	},
	/**
	 * escapeStr - string을 sql의 value로 넘기기 위해서 escape를 해주는 함수
	 *
	 * @param  {string} val escape할 value
	 * @return {string}     escape한 결과
	 */
	escapeStr: function(val) {
		if(typeof val === 'string') { return '"'+val.replace(/\\/g,'\\\\').replace(/"/g,'\\"')+'"';}
		else return val;
	},
	/**
	 * parseInt - object의 특정 필드들을 한꺼번에 parseInt를 해주는 함수.
	 *
	 * @param  {object} data      필드를 parseInt할 object
	 * @param  {array} intFields int로 변환을 시도할 field 이름 array
	 * @return {object}           변환된 객체
	 */
	parseInt: function(data, intFields) {
		return _.assign(data, _.mapValues(_.pick(data,intFields),function(val){return parseInt(val);}));
	},

	/**
	 * insertQuery - object 데이터를 기준으로 데이터 insert를 하는 기본 sql을 만들어주는 함수
	 *
	 * @param  {string} tableName   테이블명
	 * @param  {array} data        테이블에 넣을 데이터 array
	 * @param  {array} primaryKeys primary key 리스트. 이 키를 기준으로 duplicate가 자동으로 추가됨.
	 * @return {string}             sql string
	 */
	insertQuery: function(tableName, data, primaryKeys) {
		return 'INSERT INTO {tableName} ({fieldStr}) VALUE ({valueStr}) ON DUPLICATE KEY UPDATE {duplicate}'.format({
			tableName: tableName,
			fieldStr: _.keys(data).join(','),
			valueStr: _.values(_.mapValues(data, this.escapeStr)).join(','),
			duplicate: _.map(_.pairs(_.mapValues(_.omit(data, primaryKeys), this.escapeStr)),function(val){return val.join("=");}).join(',')
		});
	}
};

// add mysql continuous class
/**
 * continuous - mysql continuous 객체의 생성자. mysql continuous는 끊어지면 계속 연결을 다시 시도하여 계속 connection을 유지하도록 해주는 헬퍼.
 *
 * @param  {object} db_config    db 연결정보
 * @param  {boolean} printDisable query print를 끌지 여부
 * @return {undefined}
 */
z.mysql.continuous = function(db_config, printDisable) {
	this.db_config = db_config;
	this.conn = null;
	this.printDisable = printDisable;
	this.connect();
};

/**
 * continuous.prototype.query - 쿼리를 수행하는 함수. 연결이 안된 경우 1초마다 재시도를 수행함.
 *
 * @param  {string} query 실행할 쿼리
 * @param  {function} cb    callback
 * @return {undefined}
 */
z.mysql.continuous.prototype.query = function(query, cb) {
	if(this.conn !== null) {
		this.conn.query(query, cb);
	} else {
		var _this = this;
		setTimeout(function(){
			_this.query(query, cb);
		}, 1000);
	}
};

/**
 * continuous.prototype.connect - 끊어지지 않도록 재접속을 하는 db connector
 *
 * @return {undefined}
 */
z.mysql.continuous.prototype.connect = function() {
	var mysql = require('mysql');
	var _this = this;
	function handleDisconnect() {
		var connection = mysql.createConnection(_this.db_config); // Recreate the connection, since

		// log query
		connection.config.queryFormat = function(query, value) {
			if(!_this.printDisable) z.p(query);
			return query;
		};

		// the old one cannot be reused.
		connection.connect(function(err) {              // The server is either down
			if(err) {                                     // or restarting (takes a while sometimes).
				z.p('error when connecting to db:', err);
				setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
			} else {                                     // to avoid a hot loop, and to allow our node script to
				_this.conn = connection;
			}
		});                                     // process asynchronous requests in the meantime.

		// If you're also serving http, display a 503 error.
		connection.on('error', function(err) {
			z.p('db error', err);
			if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
				_this.conn = null;
				handleDisconnect();                         // lost due to either server restart, or a
			} else {                                      // connnection idle timeout (the wait_timeout
				throw err;                                  // server variable configures this)
			}
		});
	}
	handleDisconnect();
};

// aws ses로 메일을 쉽게 보내기 위한 helper들
z.mail = {
	saved : null,

	/**
	 * load - aws 설정 파일을 로드하여 ses 환경을 구성함.
	 *
	 * @param  {string} awsconfig aws의 환경파일 path
	 * @return {undefined}
	 */
	load : function(awsconfig) {
		var path = require('path');
		var AWS = require('aws-sdk');
		AWS.config.loadFromPath(awsconfig);
		this.saved = new AWS.SES({apiVersion: '2010-12-01'});
	},

	/**
	 * getEmailParams - ses용 email parameter의 구성 helper
	 *
	 * @param  {string} to      보낼 주소
	 * @param  {string} subject 제목
	 * @param  {string} content 내용
	 * @return {object}         ses용 parameter
	 */
	getEmailParams : function(to, subject, content) {
		return {
			Destination: { // required
				ToAddresses: [
					to
					]
			},
			Message: { // required
				Body: { // required
					Text: {
						Data: content
					}
				},
				Subject: { // required
					Data: subject
				}
			},
			Source: 'emotionallink.cto@gmail.com' // required
		};
	},

	/**
	 * send - ses로 메일을 발송함
	 *
	 * @param  {string} to      도착 주소
	 * @param  {string} subject 제목
	 * @param  {string} content 내용
	 * @param  {function} cb      callback
	 * @return {undefined}
	 */
	send : function(to, subject, content, cb) {
		if(this.saved) {
			this.saved.sendEmail(z.mail.getEmailParams(to, subject, content), cb);
		} else {
			z.p('please load aws config first before mail send');
		}
	},

	/**
	 * alarm - 받는 주소를 정해서 alarm용 helper가 좀 더 단순하게 구성되도록 한 함수
	 *
	 * @param  {string} subject 제목
	 * @param  {string} content 내용
	 * @param  {function} cb      callback
	 * @return {undefined}
	 */
	alarm : function(subject, content, cb) {
		this.send('emotionallink.cto@gmail.com', subject, content, cb);
	}
};

// 큐를 구현한 구현체인데, 현재는 이것을 사용하지 않고, z.aq를 이용하여 하는 것을 바뀌어있음
z.queue = {
	queue : [],
	state : 'stopped',
	push : function(runnable) {
		this.queue.push(runnable);
	},
	run: function() {
		var _this = this;
		setInterval(function(){
			if(_this.state !== 'running') {
				if(_this.queue.length > 0) {
					_this.state = 'running';
					_this.queue[0](function(err){
						if(err) {
							z.p(err+'::'+err.stack);
							process.exit(-1);
						} else {
							_this.queue = _this.queue.slice(1);
							_this.state='stopped';
						}
					});
				}
			}
		}, 1000);
	}
};

// solr에 접속시 편하게 하기 위해서 만든 헬퍼
z.solr = {
	client : function(solrUrl) {
		var url = require('url');
		var solr = require('solr-client');

		var solrUrlParsed = url.parse(solrUrl);
		return solr.createClient(solrUrlParsed.hostname, solrUrlParsed.port, '', solrUrlParsed.pathname);
	}
};

// 콘솔에서 입력받은 parameter를 편하게 가져오기 위한 헬퍼
z.param = {
	/**
	 * get - 특정 위치의 console parameter를 가져오고 없는 경우 msg를 출력함.
	 *
	 * @param  {object} argv   process.argv
	 * @param  {number} number 몇번째 인자를 가져올 것인지 결정(0부터 n까지). 여기서 process 정보는 제외됨.(즉 process.argv의 맨앞 2개는 무시)
	 * @param  {string} msg    인자가 없는 경우 출력할 에러메세지
	 * @return {string}        해당 인자의 value
	 */
	get: function(argv, number, msg) {
		if(argv.length < number+3) {
			z.p(msg);
			process.exit(-1);
		} else {
			return argv[number+2];
		}
	},
	/**
	 * getSafe - 특정 위치의 console parameter를 안전하게 가져옴. 없다하더라도 프로그램이 종료되지 않음. 없는 경우 null을 return.
	 *
	 * @param  {object} argv   process.argv
	 * @param  {number} number 몇번째 인자를 가져올 것인지 결정(0부터 n까지). 여기서 process 정보는 제외됨.(즉 process.argv의 맨앞 2개는 무시)
	 * @return {string|null}        해당 인자의 value
	 */
	getSafe: function(argv, number) {
		if(argv.length < number+3) {
			return null;
		} else {
			return argv[number+2];
		}
	}
};


/**
 * exec - command를 실행
 *
 * @param  {string} cmd 실행할 command
 * @param  {function} cb  callback
 * @return {undefined}
 */
z.exec = function(cmd, cb){
	var exec = require('child_process').exec;
	z.p(cmd);
	exec(cmd, cb);
};

/**
 * cb - 추가된 arguments를 다음 cb에서 사용하고 싶을 때 쓰는 함수이지만, 순서가 그 이후로 붙임(cb+origin arg+added arg)
 *
 * @param  {function} cb description
 * @return {undefined}    description
 */
z.cb = function(cb) {
	var added_args = Array.prototype.slice.call(arguments).slice(1);
	return function() {
		var args = Array.prototype.slice.call(arguments);
		cb.apply(this, args.concat(added_args));
	};
};

/**
 * pass - cb와 비슷하지만 arguments의 순서가 다름(cb+added arg+origin arg). origin arg가 있을 수도 있고 없을 수도 있으므로 pass를 쓰는 것이 더 편리함.
 *
 * @param  {function} cb callback
 * @return {undefined}
 */
z.pass = function(cb) {
	var added_args = Array.prototype.slice.call(arguments).slice(1);
	return function() {
		var args = Array.prototype.slice.call(arguments);
		if(args.length === 0) args = [null];
		cb.apply(this, args.slice(0, 1).concat(added_args).concat(args.slice(1)));
	};
};

// 함수 실행시간을 계산하기 편리한 helper
z.time = {
	start_time: null,

	/**
	 * start - 시간 계산을 시작할때 부르는 함수
	 *
	 * @return {undefined}
	 */
	start: function() {
		this.start_time = process.hrtime();
	},

	/**
	 * end - 시간 계산 끝날 때 부르는 함수. end만 여러번 부르는 경우, 이어서 시간 체크가 가능함.
	 *
	 * @param  {string} note 로그에 추가할 메세지
	 * @return {undefined}
	 */
	end: function(note) {
	    var precision = 3; // 3 decimal places
	    var elapsed = process.hrtime(this.start_time)[1] / 1000000; // divide by a million to get nano to milli
	    z.p(process.hrtime(this.start_time)[0] + " s, " + elapsed.toFixed(precision) + " ms - " + note); // print message + time
	    this.start_time = process.hrtime(); // reset the timer
	}
};

// async shorteners
// 좀더 간단히 async를 쓰려고 만든 함수

/**
 * aw - async waterfall의 축약 함수. funcArr에 넣는 func는 (cb, prev results...) 의 순서로 정의되면 됨.
 *
 * @param  {array} funcArr 실행할 함수 array
 * @param  {function} cb      callback
 * @return {undefined}
 */
z.aw = function(funcArr, cb) {
	async.waterfall(z.async.task(funcArr), function(err){
		if(err && err.aw_end) {
			var user_args = Array.prototype.slice.call(arguments).slice(1);
			cb.apply(this, [null].concat(user_args));
		} else {
			cb.apply(this, Array.prototype.slice.call(arguments));
		}
	});
};

/**
 * aw_end - waterfall을 진행하다가 뒤의 스텝을 무시하고 정상 종료하고 싶은 경우 사용. 최종 결과를 cb 다음 인자로 전달할 수 있음.
 *
 * @param  {function} cb callback
 * @return {undefined}
 */
z.aw_end = function(cb) {
	var user_args = Array.prototype.slice.call(arguments).slice(1);
	cb.apply(this, [{aw_end:true}].concat(user_args));
};

/**
 * at - z.async.task의 축약함수. 이름만 줄여놓은 것으로 역할을 동일함.
 *
 * @param  {array} tasks    function을 넣을 task array
 * @param  {function} taskFunc 추가할 task function
 * @return {undefined}
 */
z.at = function (tasks, taskFunc) {
	var user_args = Array.prototype.slice.call(arguments).slice(2);
	tasks.push(function() {
		var args = Array.prototype.slice.call(arguments);
		taskFunc.apply(this, args.slice(0, 1).concat(user_args).concat(args.slice(1)));
	});
};

/**
 * ap - async parallel의 축약 함수. funcArr의 함수는 첫번째 인자로 cb를 받아야한다.
 *
 * @param  {array} funcArr 실행할 function의 array
 * @param  {number} limit   (optional) 동시실행을 진행할 프로세스의 숫자. 없는 경우 모든 function을 동시실행
 * @param  {function} cb      callback
 * @return {undefined}
 */
z.ap = function(funcArr, limit, cb) {
	if(typeof limit === 'function') {
		cb = limit;
		async.parallel(z.async.task(funcArr), cb);
	} else {
		async.parallelLimit(z.async.task(funcArr), limit, cb);
	}
};

/**
 * as - async series의 축약 함수. funcArr의 함수는 첫번째 인자로 cb를 받아야한다.
 *
 * @param  {array} funcArr 실행할 function의 array
 * @param  {function} cb      callback
 * @return {undefined}
 */
z.as = function(funcArr, cb) {
	async.series(z.async.task(funcArr), cb);
};

/**
 * aq - async queue를 쉽게 사용하기 위한 helper class.
 *
 * @param  {number} limit 최대 동시실행 숫자.
 * @return {undefined}
 */
z.aq = function(limit) {
	this.limit = limit?limit:9007199254740992;
	this.q = async.queue(function(task, cb){
		task(cb);
	}, this.limit);
};

/**
 * aq.prototype.push - 큐에서 실행할 함수 추가.
 *
 * @param  {function} task 실행할 함수. 첫번째 인자로 cb를 받아야한다.
 * @return {undefined}
 */
z.aq.prototype.push = function(task) {
	this.q.push(task);
};

/**
 * aq.prototype.complete - 큐에 있는 모든 함수를 실행하고 종료.
 *
 * @param  {function} cb callback
 * @return {undefined}
 */
z.aq.prototype.complete = function(cb) {
	if(this.q.length() > 0) {
		this.q.drain = function() {
			cb();
		};
	} else {
		cb();
	}
};

// process end

/**
 * end - 프로세스 종료를 짧게 하려고 만든 wrapper
 *
 * @param  {type} err description
 * @return {type}     description
 */
z.end = function(err) {
	if(z.e(err)) process.exit(-1);
	else process.exit();
};

// random string

/**
 * id - random한 문자열을 원하는 길이로 만들어주는 함수
 *
 * @param  {type} len     description
 * @param  {type} charSet description
 * @return {type}         description
 */
z.id = function(len, charSet) {
	charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var randomString = '';
	for (var i = 0; i < len; i++) {
		var randomPoz = Math.floor(Math.random() * charSet.length);
		randomString += charSet.substring(randomPoz,randomPoz+1);
	}
	return randomString;
};

// ssh

/**
 * ssh - ssh로 서버에 연결하여 command를 실행하는 것을 도와주는 helper class
 *
 * @param  {string} key      ssh key파일의 path
 * @param  {string} username ssh username
 * @return {undefined}
 */
z.ssh = function(key, username) {
	this.key = key;
	this.username = username;
};

 /**
  * ssh.prototype.cmd - ssh helper class로 지정한 key와 username을 가지고 특정 서버에 접속하여 command를 실행하고 결과를 callback함
  *
	* @param  {string} addr 접속할 서버 주소
  * @param  {string} cmd  실행할 command
  * @param  {boolean} sudo_mod (optional)sudo로 실행할지 여부. 없으면 false.
  * @param  {function} cb       callback
  * @return {undefined}
  */
z.ssh.prototype.cmd = function(addr, cmd, sudo_mod, cb) {
	if(typeof sudo_mod === 'function') {
		cb = sudo_mod;
		sudo_mod = false;
	}

	var ssh_cmd = "ssh {0} {4} -o StrictHostKeyChecking=no {1}@{2} '{5}{3}{6}' {7}".format(this.key ? "-i "+this.key:"", this.username, addr, cmd.replace(/'/g,"\\'"), sudo_mod?"-tt":"", sudo_mod?"stty raw -echo;":"", sudo_mod?"| cat":"", sudo_mod?"< <(cat)":"");

    z.p("{0} - start {1}".format(addr, cmd));

    var params = ["-c", ssh_cmd];
    var ssh_spawn = spawn("bash", params);
    var stdout = "";
    var stderr = "";
    ssh_spawn.stdout.on('data', function (data) {
        stdout+=z.p(data.toString());
    });
    ssh_spawn.stderr.on('data', function (data) {
        stderr+=z.err(data.toString());
    });
    ssh_spawn.on('close', function (code) {
        cb(code===0?null:{cmd:ssh_cmd,code:code,err:stderr}, stdout, stderr);
        z.p("{0} - end {1}".format(addr, cmd));
    });
};

// parallel tasks using data array

/**
 * ap_task - 특정 데이터 array를 기준으로 task를 구성하고 이를 parallel로 실행한다. z.at, z.ap의 축약버젼.
 *
 * @param  {array} datas task로 구성할 데이터 array
 * @param  {function} job   task function. 인자로 (data, cb)를 가진다.
 * @param  {callback} cb    callback
 * @param  {number} limit (optional) 동시에 실행할 최대 프로세스수
 * @return {undefined}
 */
z.ap_task = function(datas, job, cb, limit) {
    var tasks = [];
    _.forEach(datas, function(data) {
        z.at(tasks, function(cb, data) {
            async.setImmediate(function(){
            	job(data, cb);
            });
        }, data);
    });
    z.ap(tasks, limit?limit:cb, limit?cb:null);
};

/**
 * aw_task - 특정 데이터 array를 기준으로 task를 구성하고 이를 waterfall로 실행한다. z.at, z.aw의 축약버젼.
 *
 * @param  {array} datas task로 구성할 데이터 array
 * @param  {function} job   task function. 인자로 (data, cb)를 가진다.
 * @param  {callback} cb    callback
 * @return {undefined}
 */
z.aw_task = function(datas, job, cb) {
    var tasks = [];
    _.forEach(datas, function(data) {
        z.at(tasks, function(cb, data) {
        	async.setImmediate(function(){
            	job(data, cb);
            });
        }, data);
    });
    z.aw(tasks, cb);
};

// retry

/**
 * retry - 특정 job을 실패하지 않을 때까지 반복하여 수행한다.
 *
 * @param  {function} func     실행할 job
 * @param  {number} retryNum 반복 시도를 할 최대 수
 * @param  {function} cb       callback
 * @param  {number} tryNo    (optional)현재의 시도차수. 없는 경우 첫시도
 * @return {undefined}
 */
z.retry = function(func, retryNum, cb, tryNo) {
	if(!tryNo) tryNo = 0;
	func(function(err) {
		if(err) {
			if(tryNo < retryNum) {
				z.retry(func, retryNum, cb, ++tryNo);
			} else {
				cb(err);
			}
		} else {
			cb.apply(this, Array.prototype.slice.call(arguments));
		}
	});
};

// file write

/**
 * write - 파일에 특정 string을 write하기 편하게 만드는 helper
 *
 * @param  {string} fd  쓸 파일의 fd
 * @param  {string} str 쓸 내용
 * @param  {function} cb  callback
 * @return {undefined}
 */
z.write = function(fd, str, cb) {
	var buf = new Buffer(str);
	fs.write(fd, buf, 0, buf.length, null, cb);
};

// basic cluster

/**
 * cluster - cluster를 clusterNum 만큼으로 구성하고 child가 죽는 경우 재실행되도록 설정한 기본 클러스터 구성을 만들어줌.
 *
 * @param  {object} cluster    node의 cluster 객체
 * @param  {number} clusterNum 만들 child의 수
 * @param  {callback} cb         callback
 * @return {array}       worker의 array
 */
z.cluster = function(cluster, clusterNum, cb) {
	var workers = [];

	// Fork workers.
	for (var i = 0; i < clusterNum; i++) {
		var worker = cluster.fork();
		workers.push(worker);
	}

	cluster.on('exit', function(worker, code, signal) {
		z.p('worker ' + worker.process.pid + ' died');
		if(typeof code === "undefined" || code !== 0) {
			z.p('signal = '+signal);
			process.exit(typeof code === "undefined" ? -1 : code);
		} else {
			_.remove(workers, worker);

			if(workers.length === 0) {
				z.p("All workers are died successfully");
				if(cb) cb();
				else z.end();
			}
		}
	});

	return workers;
};

// stream

/**
 * stream - 파일을 stream으로 읽어 readline을 하려할 때, 해야하는 기본 셋팅을 해줌. 리턴받은 stream 객체에 on('readline')을 활용하여 이벤트를 받으면 됨.
 *
 * @param  {string} fileName 읽을 파일 path
 * @param  {function} cb       callback
 * @return {object}          readline stream 객체
 */
z.stream = function(fileName, cb) {
	var readline = require('readline');
	var stream = null;
	if(typeof fileName === "string") {
		stream = readline.createInterface({
			input:fs.createReadStream(fileName),
			output:null,
			terminal:false
		});
	} else {
		stream = readline.createInterface({
			input:fileName,
			output:null,
			terminal:false
		});
	}
	stream.on('close', cb);
	stream.on('error', cb);
	return stream;
};

// get private ip

/**
 * getPrivateIp - 현재 서버의 private ip를 얻어옴.
 *
 * @return {string|null}  현재 서버의 private ip
 */
z.getPrivateIp = function() {
    var os = require('os');
    var ifaces = os.networkInterfaces();
    var keys = Object.keys(ifaces);
    for(var i=0;i<keys.length;i++) {
        var ifaceg = ifaces[keys[i]];
        for(var j=0;j<ifaceg.length;j++) {
            var iface = ifaceg[j];
            if ('IPv4' !== iface.family || iface.internal !== false) continue;
            else return iface.address;
        }
    }
    return null;
};

module.exports = function(othermodule) {
	if(othermodule) { // 하위에 있는 다른 파일을 로드하고 싶으면 require('zeninode')({submodule name})으로 한다.
		return require('./'+othermodule);
	} else { // 보통 require('zeninode')();로 호출하여 extendString을 기본 실행하도록 셋팅한다.
		require('stringformat').extendString();
		return z;
	}
};
