
var request = require('request');
var z = require('zeninode')();
var async = require('async');

var V = function(url) {
	this.url = url;
};

// helper
V.prototype.val = function(val) {
	if(val && typeof val === 'string' && val.replace(/ /g, '')=== '') {
		return null;
	} else {
		return val;
	}
};

// rule checkers
V.prototype.minlength = function(cb, data) {
	cb(data.val.length >= data.ruleVal ? null : data);
};

V.prototype.maxlength = function(cb, data) {
	cb(data.val.length <= data.ruleVal ? null : data); 
};

V.prototype.email = function(cb, data) {
	cb(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(data.val) ? null : data);
};

V.prototype.remote = function(cb, data) {
	request.get("{url}{ruleVal}?{field}={val}".format(data), function(err, res, body){
		cb(body === "true" ? null : data);
	});
};

V.prototype.test = function(fields_rules, obj, cb) {
	var tasks = [];
	for(var field in fields_rules) {
		var rules = fields_rules[field];
		var value = V.prototype.val(obj[field])
		if(value) {
			for(var rule in rules) {
				if(rule !== "required") {
					var data = {val:value, ruleVal:rules[rule], rule:rule, field:field, url:this.url};
					z.async.taskInjector(tasks, this[rule], data);
				}
			}
		} else {
			if(rules.required) {
				cb({
					data: {val:value, ruleVal:rules.required, field:field}
				});
				return ;
			}
		}
	}
	async.waterfall(tasks, cb);
};

module.exports = V;
